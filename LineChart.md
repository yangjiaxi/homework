### [Recharts](http://recharts.org/examples/#/zh-CN)

- [API](http://recharts.org/examples/#/zh-CN/api)

1. 渲染速度比较快

2. 容易与`react`配合

3. Api和配置相对简洁

4. 图表下可配置[Brush](http://recharts.org/examples/#/zh-CN/api/Brush)功能,类似`HighCharts`的range select功能

5. 可配置多条Y轴,[issue](https://github.com/recharts/recharts/issues/174)

6. 可基本满足现有折线图需求


