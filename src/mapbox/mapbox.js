import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

import HeatMap from './maps/HeatMap'
import Markers from './maps/Markers'
import Line from './maps/Line'
class MapBox extends React.Component {



  render() {
    return (
      <Router

      >
        <div>
          <ul className='sub-menu'>
            <li><Link to="/HeatMap">HeatMap</Link></li>
            <li><Link to="/Markers">Markers</Link></li>
            <li><Link to="/Line">Line</Link></li>
          </ul>
          <hr/>
          <Route exact path="/HeatMap" component={HeatMap}/>
          <Route exact path="/Markers" component={Markers}/>
          <Route exact path="/Line" component={Line}/>

        </div>
      </Router>
    )
  }
}

export default MapBox