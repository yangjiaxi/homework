import React from 'react'
import axios from 'axios'

const mapboxgl = window.mapboxgl
const token = 'pk.eyJ1IjoieWFuZ2ppYXhpIiwiYSI6ImNqNzl0NTN0MzA2bmwzMnFtcXJ4MDNkbzMifQ.O2IKwcIyEBlcxzcLSukBKA'
mapboxgl.accessToken = token
let map = null

const url = 'https://raw.githubusercontent.com/uber-common/deck.gl-data/master/examples/scatterplot/manhattan.json'

class Markers extends React.Component {
  componentDidMount() {
    /*   axios.get('/d.json')
         .then(res => {
           const data = []
           for (let i of res.data) {
             try {
               if (i.last_valid_gps.latitude < 200 && i.last_valid_gps.longitude < 200) {
                 data.push(i)
               }
             } catch (err) {

             }
           }
           this.drawMap(data)
         })*/

    axios.get('/data1w.json')
      .then(res => {

        this._drawMap(res.data)
      })

  }

  _drawMap(data) {
    console.log(data)
    map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/light-v9',
      center: data[0],
      zoom: 8
    })
    data.map(v => {
      console.log(v)
      const el = document.createElement('div')
      el.id = 'marker'
      new mapboxgl.Marker(el)
        .setLngLat(v)
        .addTo(map)
    })
  }

  drawMap(data) {
    map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/light-v9',
      center: [data[0].last_valid_gps.longitude, data[0].last_valid_gps.latitude],
      zoom: 8
    })
    const coordinates = []
    data.map(v => {
      const loc = [v.last_valid_gps.longitude, v.last_valid_gps.latitude]
      // create the popup
      const popup = new mapboxgl.Popup()
        .setHTML(`<p style="padding: 10px;">${v.id}</p>`)

// create DOM element for the marker
      const el = document.createElement('div')
      el.id = 'marker'

      coordinates.push(loc)

// create the marker
      new mapboxgl.Marker(el)
        .setLngLat(loc)
        .setPopup(popup) // sets a popup on this marker
        .addTo(map)
    })
    const bounds = coordinates.reduce(function (bounds, coord) {
      return bounds.extend(coord);
    }, new mapboxgl.LngLatBounds(coordinates[0], coordinates[0]));

    map.fitBounds(bounds, {
      padding: 20
    })

  }

  render() {
    return (
      <div id='map'></div>
    )
  }
}

export default Markers