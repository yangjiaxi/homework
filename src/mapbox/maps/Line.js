import React from 'react'
import axios from 'axios'

const mapboxgl = window.mapboxgl
const token = 'pk.eyJ1IjoieWFuZ2ppYXhpIiwiYSI6ImNqNzl0NTN0MzA2bmwzMnFtcXJ4MDNkbzMifQ.O2IKwcIyEBlcxzcLSukBKA'
mapboxgl.accessToken = token
let map = null



const geojson = {
  "type": "FeatureCollection",
  "features": [{
    "type": "Feature",
    "geometry": {
      "type": "LineString",
      "properties": {},
      "coordinates": [
        [-77.0366048812866, 38.89873175227713],
        [-77.03364372253417, 38.89876515143842],
        [-77.03364372253417, 38.89549195896866],
        [-77.02982425689697, 38.89549195896866],
        [-77.02400922775269, 38.89387200688839],
        [-77.01519012451172, 38.891416957534204],
        [-77.01521158218382, 38.892068305429156],
        [-77.00813055038452, 38.892051604275686],
        [-77.00832366943358, 38.89143365883688],
        [-77.00818419456482, 38.89082405874451],
        [-77.00815200805664, 38.88989712255097]
      ]
    }
  }]
}

class Line extends React.Component {
  componentDidMount() {
    axios.get('/e.json')
      .then(res => {
        this.drawMap(res.data)
      })
  }

  drawMap(data) {
    map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/light-v9',
      center: [-77.0214, 38.8970],
      zoom: 12
    })

    map.on('load', function () {
      map.addLayer({
        "id": "LineString",
        "type": "line",
        "source": {
          "type": "geojson",
          "data": geojson
        },
        "layout": {
          "line-join": "round",
          "line-cap": "round"
        },
        "paint": {
          "line-color": "#BF93E4",
          "line-width": 5
        }
      })
    })
  }


  render() {
    return (
      <div id='map'></div>
    )
  }
}

export default Line