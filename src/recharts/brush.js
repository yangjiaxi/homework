import React from 'react'
import {LineChart, Line, Brush, XAxis, YAxis, CartesianGrid, Tooltip,Legend} from 'recharts'
import axios from 'axios'


class _Brush extends React.Component {

  constructor() {
    super()
    this.state = {
      data: []
    }
    axios.get('/d.json')
      .then(res => {
        this.setState({
          data: res.data
        })
      })
  }

  render() {
    const {data} = this.state
    return (
      <LineChart width={1000} height={600} data={data} syncId="anyId"
                 margin={{top: 10, right: 30, left: 0, bottom: 0}}>
        <XAxis dataKey='updated_at'/>
        <YAxis
          type='number'
          domain={[3.6, 4]}
          yAxisId='y1'
          label='电量'
        />
        <YAxis
          type='number'
          orientation='right'
          yAxisId='y2'
          label='温度'
        />
        <YAxis
          type='number'
          orientation='right'
          yAxisId='y3'
          label='水平'
        />
        <YAxis
          type='number'
          yAxisId='y4'
          label='信号强度'
        />
        <YAxis
          type='number'
          yAxisId='y5'
          label='气压'
        />

        <Legend />
        <Tooltip/>

        <Line type='monotone' dataKey='battery_voltage' stroke='#82ca9d' fill='#82ca9d' yAxisId='y1' name='电量'/>
        <Line type='monotone' dataKey='temperature' stroke='#82129d' dot={false} yAxisId='y2' name='气压'/>
        <Line type='monotone' dataKey='horizontal' stroke='#22129d' dot={false} yAxisId='y3' name='电量'/>
        <Line type='monotone' dataKey='signal_strength' stroke='#02129d' dot={false} yAxisId='y4' name='信号强度'/>
        <Line type='monotone' dataKey='pressure' stroke='#02329d' dot={false} yAxisId='y5' name='气压'/>

        <Brush/>
      </LineChart>
    )
  }
}

export default _Brush

const testLabel = () => (
  <div style={{color: 'red'}}>信号强度</div>
)

