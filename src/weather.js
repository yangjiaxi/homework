import React from 'react'
import axios from 'axios'

class Weather extends React.Component {
  constructor() {
    super()
    this.state = {
      data: []
    }
    axios.get('/w.json')
      .then(res => {
        console.log(res.data)
        this.setState({
          data: res.data.showapi_res_body.dayList.splice(0, 7)
        })
      })
  }

  render() {
    const {data} = this.state
    console.log(data)
    return (
      <ul className='weather-list'>
        {
          data.map(v => {
            return (
              <li
                key={v.daytime}
              >
               <span>
                 {date(v.daytime)}
               </span>
                <span>
                  <img src={v.day_weather_pic} alt=""/>
                  <img src={v.night_weather_pic} alt=""/>
                </span>

                {
                  v.day_weather === v.night_weather ?
                    (<span>{v.day_weather}</span>) :
                    (<span>{v.day_weather}~{v.night_weather}</span>)
                }


                {
                  v.day_air_temperature === v.night_air_temperature ?
                    (<span>{v.day_air_temperature}℃</span>) :
                    (<span>{v.day_air_temperature}℃~{v.night_air_temperature}℃</span>)
                }

                <span>
                  {v.day_wind_power}
                </span>
              </li>
            )
          })
        }
      </ul>
    )
  }
}

export default Weather


const date = v => {
  const y = v.substring(0, 4)
  const m = v.substring(4, 6)
  const d = v.substring(6, 8)
  return `${y}年${m}月${d}日`
}