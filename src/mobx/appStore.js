import {observable, action} from 'mobx'

let store = observable({
  number: 1
})
store.change = action((name, value) => {
  store[name] = value
})

export default store