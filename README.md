### [Recharts](http://recharts.org/examples/#/zh-CN)

- [API](http://recharts.org/examples/#/zh-CN/api)

1. 渲染速度比较快

2. 容易与`react`配合

3. Api和配置相对简洁

4. 图表下可配置[Brush](http://recharts.org/examples/#/zh-CN/api/Brush)功能,类似`HighCharts`的range select功能

5. 可配置多条Y轴,[issue](https://github.com/recharts/recharts/issues/174)

6. 可基本满足现有折线图需求

7. 基本demo在`src/recharts/brush.js`下

8. 测试所用数据在`public`下


### [dygraphs](http://dygraphs.com/)

- [API](http://dygraphs.com/jsdoc/symbols/Dygraph.html)

1. 渲染速度尚可

2. 没有与React相关联的库

3. 数据源格式要求严格(多支持csv格式)

### [vega](https://vega.github.io/vega/)

- [DOC](https://vega.github.io/vega/docs/)

1. 界面简洁,美观,大方

2. 支持的图标类型多 

3. 配置项极其复杂 [DEMO](https://vega.github.io/vega/examples/bar-chart/)

4. 没有与React相关联的库

### [NVD3.js](http://nvd3.org/index.html)

1. 缺陷:当数据量大时,没有rangeSelect功能


### [Highcharts](https://www.highcharts.com/)与[Echarts](http://echarts.baidu.com/)

1. 图标类型多而全

2. 文档清楚

3. 与[Recharts](http://recharts.org/examples/#/zh-CN)相比,在折线图方面没有明显优势

4. 需要直接操作DOM配合`React`的生命周期函数来添加图表

